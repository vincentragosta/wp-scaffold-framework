#!/usr/bin/env bash

while getopts "d" option
do
 case "${option}"
 in
 d)
   DRYRUN_OPTION=1
   ;;
 esac
done

DRYRUN=${DRYRUN_OPTION:-0}

BASEDIR=$(pwd)
THEMESDIR="$BASEDIR/wp-content/themes"
echo "\nWelcome to the Vincent Ragosta theme startup script. Please enter some information before the script is officially run.\n"
read -p "Project title: " SITE_NAME
if [ -z "$SITE_NAME" ]
then
  echo "\nYou must enter a project title.\n"
  exit
fi

DEFAULT_THEME_NAME=$(echo "$SITE_NAME" | tr ' ' - | tr '[:upper:]' '[:lower:]')
read -p "Theme directory name: [${DEFAULT_THEME_NAME}] " THEME
THEME=${THEME:-${DEFAULT_THEME_NAME}}
THEMEDIR="$THEMESDIR/$THEME"

read -p "Git repo for ${SITE_NAME}: " GIT_REPO
if [ -z "$GIT_REPO" ]
then
    echo "\nYou must enter a git repository.\n"
    exit
else
  VAR=$(basename $GIT_REPO)
  PACKAGE_NAME="${VAR//.git/ }"
fi

if [[ DRYRUN -eq 1 ]]
then
    echo $PACKAGE_NAME
    exit
fi

php init-replace.php {theme-name} $PACKAGE_NAME composer.json
echo "\nFetching WP salts\n"
curl -s 'https://api.wordpress.org/secret-key/1.1/salt' | php init-replace.php //{salts}// wp-config.php

composer install

php init-replace.php "WP Scaffold" "$SITE_NAME" "$THEMEDIR/style.css"
mv wp-content/themes/wp-scaffold-theme "wp-content/themes/$THEME"

composer remove vincentragosta/wp-scaffold-theme --dev

rm -rf "$THEMEDIR/.git"
rm -rf "$THEMEDIR/composer.json"
rm -rf "$THEMEDIR/composer.lock"
rm -rf "./.git"
rm -f "./start-project.sh" "./init-replace.php"

if [[ ! -z "$GIT_REPO" ]]
then
    git init
    git remote add origin $GIT_REPO
    git checkout -b develop
    git add --all
    git commit -m "initial commit from the startup script"
    git push --set-upstream origin develop
fi

composer develop